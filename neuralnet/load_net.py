#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' 
import keras
import time
from pickle import load
from sklearn.preprocessing import RobustScaler, Normalizer
from plot_4_samples import plot_4_samples, plot_4_samples_with_pred

print('Loading net for visualization')

##########################################
# settings
##########################################

data_dir = os.path.expanduser('~/labeling/training')
network_dir = os.path.expanduser('~/network')


settings = np.load(data_dir+'/settings.npy', allow_pickle=True).item() 

only_nc = settings['only_nc']
augment_noise = settings['augment_noise']
augment = settings['augment']
mode = settings['mode']

layer_type = settings['layer_type'] 
size = settings['size'] 
arch = '23' #settings['arch']
n_epochs = settings['n_epochs']
save = True


cam_dim = 424
lid_dim = 360
det_dim = 4

print(settings)
settings_name = str(layer_type) + '_' + str(size) + '_' + str(arch) + '_' + str(n_epochs) + 'epochs_' + str(only_nc) + str(augment_noise) + str(augment) + str(mode)

save_dir = network_dir + '/plots/' + settings_name 


i_l = 1
i_c = 425

##########################################
# Load data
##########################################
x_val = np.load(data_dir + '/x_val.npy')
y_val = np.load(data_dir + '/y_val.npy')


print(x_val.shape, y_val.shape)

print(x_val[0, 0:5])
print(y_val[0, 0:5])



# Load model #
##########################################
model = keras.models.load_model(network_dir + '/models/' + settings_name)
history = np.load(network_dir + '/history/' + settings_name + '.npy',allow_pickle='TRUE').item()



##########################################
# Select data
##########################################
# only use x_val: already scaled
print('--------Getting scaled x samples----------')
nc_sample_ind = np.where(x_val[:,0]==1)[0]
nc_sample = x_val[nc_sample_ind[0], :]
print(nc_sample[0:5])

cam_error_ind = np.where(x_val[:,0]==2)[0]
cam_sample = x_val[cam_error_ind[0], :]
print(cam_sample[0:5])

lid_error_ind = np.where(x_val[:,0] ==3)[0]
lid_sample = x_val[lid_error_ind[0], :]
print(lid_sample[0:5])

both_errors_ind = np.where(x_val[:,0] ==4)[0]
both_sample = x_val[both_errors_ind[0], :]
print(both_sample[0:5])

plot_4_samples([nc_sample, cam_sample, lid_sample, both_sample], save_dir , 'scaled_x_samples', save = save)



##########################################
# Get non scaled data (y_val)
##########################################
# only use x_val: already scaled
print('--------Getting unscaled, clean y samples----------')
y_nc_sample = y_val[nc_sample_ind[0], :]
print(y_nc_sample[0:5])

y_cam_sample = y_val[cam_error_ind[0], :]
print(y_cam_sample[0:5])

y_lid_sample = y_val[lid_error_ind[0], :]
print(y_lid_sample[0:5])

y_both_sample = y_val[both_errors_ind[0], :]
print(y_both_sample[0:5])

plot_4_samples([y_nc_sample, y_cam_sample, y_lid_sample,y_both_sample], save_dir , 'clean_y_samples', save = save)



##########################################
# Predict data
##########################################
# only use x_val: already scaled
print('--------Prediction on x samples----------')


predict_nc_input = [np.reshape(nc_sample[i_l:i_c], (1,424)), np.reshape(nc_sample[i_c:], (1,360))]
predict_nc = model.predict(predict_nc_input)
print(predict_nc[0], predict_nc[1][0,0:2])

predict_cam_input = [np.reshape(cam_sample[i_l:i_c], (1,424)), np.reshape(cam_sample[i_c:], (1,360))]
predict_cam = model.predict(predict_cam_input)
print(predict_cam[0], predict_cam[1][0,0:2])

predict_lid_input = [np.reshape(lid_sample[i_l:i_c], (1,424)), np.reshape(lid_sample[i_c:], (1,360))]
predict_lid = model.predict(predict_lid_input)
print(predict_lid[0], predict_lid[1][0,0:2])

predict_both_input = [np.reshape(both_sample[i_l:i_c], (1,424)), np.reshape(lid_sample[i_c:], (1,360))]
predict_both = model.predict(predict_both_input)
print(predict_both[0], predict_both[1][0,0:2])


plot_4_samples_with_pred([predict_nc, predict_cam, predict_lid, predict_both], save_dir , 'reconstructed_samples', save = save)


# ##########################################
# # Plot the metrics
# ##########################################


print(history.keys())
fig = plt.figure('Losses', figsize=(20,10))
plt.subplot(2,3,1)
plt.plot(history['cam_output_loss'])
plt.plot(history['val_cam_output_loss'])
plt.title('Camera loss')
plt.ylabel('loss')
plt.subplot(2,3,2)
plt.plot(history['lid_output_loss'])
plt.plot(history['val_lid_output_loss'])
plt.title('Lidar loss')
plt.subplot(2,3,3)
plt.plot(history['detection_output_loss'])
plt.plot(history['val_detection_output_loss'])
plt.title('Detection loss')
plt.subplot(2,3,4)
plt.plot(history['cam_output_mean_squared_error'])
plt.plot(history['val_cam_output_mean_squared_error'])
plt.title('Camera mse')
plt.ylabel('MSE')
plt.xlabel('epoch')
plt.subplot(2,3,5)
plt.plot(history['lid_output_mean_squared_error'])
plt.plot(history['val_lid_output_mean_squared_error'])
plt.title('Lidar mse')
plt.xlabel('epoch')
plt.subplot(2,3,6)
plt.plot(history['detection_output_mean_squared_error'])
plt.plot(history['val_detection_output_mean_squared_error'])
plt.title('Detection mse')
plt.xlabel('epoch')
if save:
    plt.savefig(save_dir+'/losses.jpg')
else:
    plt.show()


fig = plt.figure('Detection', figsize=(20,10))
plt.subplot(2,2,1)
plt.plot(history['detection_output_categorical_accuracy'])
plt.plot(history['val_detection_output_categorical_accuracy'])
plt.legend(['train', 'val'])
plt.title('detection_output_categorical_accuracy')

plt.subplot(2,2,2)
plt.plot(history['detection_output_recall'])
plt.plot(history['val_detection_output_recall'])
plt.legend(['train', 'val'])
plt.title('detection_output_recall')

plt.subplot(2,2,3)
plt.plot(history['detection_output_loss'])
plt.title('Detection loss')

plt.subplot(2,2,4)
plt.plot(history['detection_output_precision'])
plt.plot(history['val_detection_output_precision'])
plt.legend(['train', 'val'])
plt.title('detection_output_precision')
plt.xlabel('epoch')

if save:
    plt.savefig(save_dir+'/detection.jpg')
else:
    plt.show()

