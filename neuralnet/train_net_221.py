#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' 
from keras.layers import Dense, Input, Concatenate
from keras.models import Model
import tensorflow as tf
from build_model_221 import build_model, build_detector
from keras.utils.vis_utils import plot_model
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.utils import to_categorical
import keras
from pickle import load
from plot_4_samples import predict_input



print('Training of autoencoder')

# # Settings
# ###############################################################

to_dir = '~/network'
data_dir = '~/labeling/training'

settings = np.load(os.path.expanduser('~/labeling/training/')+'settings.npy', allow_pickle=True).item() 

only_nc = settings['only_nc']
augment_noise = settings['augment_noise']
augment = settings['augment']
mode = settings['mode']

layer_type = 'conv'  #'conv' or 'dense'
<<<<<<< HEAD
size = 'small'  #'small' or 'big'
=======
size = 'big'  #'small' or 'big'
>>>>>>> ef4ce3cbadea865619cd2e9ff4d42f1d77a6e2f5
arch = '221'  #'23' or '221'
n_epochs = 10


cam_dim = 424
lid_dim = 360
det_dim = 4

settings_name = str(layer_type) + '_' + str(size) + '_' + str(arch) + '_' + str(n_epochs) + 'epochs_' + str(only_nc) + str(augment_noise) + str(augment) + str(mode)

# Update dict
settings['layer_type'] = layer_type  #'conv' or 'dense'
settings['size'] = size #'small' or 'big'
settings['arch'] = arch  #'23' or '221'
settings['n_epochs'] = n_epochs
print(settings)

# update settings file
np.save(os.path.expanduser('~/labeling/training/')+'settings.npy', settings) 

# # Build model
# ###############################################################

model = build_model(layer_type, size, arch, cam_dim, lid_dim, det_dim)

plot_model(model, to_file=os.path.expanduser(to_dir)+'/model_plots/'+str(layer_type)+'_'+str(size)+'_'+str(arch)+'.png', show_shapes=True, show_layer_names=True)


# # Training specifics
# ###############################################################

# Specify optimisers and loss functions for each output
lr_schedule = keras.optimizers.schedules.ExponentialDecay(
    initial_learning_rate=1e-2,
    decay_steps=10000,
    decay_rate=0.9)

optimizer = tf.keras.optimizers.Adam(
    learning_rate=lr_schedule, beta_1=0.9, beta_2=0.999, epsilon=1e-06, amsgrad=False,
    name='Adam'
)


model.compile(optimizer=optimizer, 
        loss={'cam_output':'mse', 'lid_output':'mse'},
        metrics={
            'cam_output':'MeanSquaredError',
            'lid_output':'MeanSquaredError'})





# # Load data
# ###############################################################
# # Rightclick on file in tree to get relative or absolute path

x_train = np.load(os.path.expanduser(data_dir)+'/x_train.npy')
x_val = np.load(os.path.expanduser(data_dir)+'/x_val.npy')

y_train = np.load(os.path.expanduser(data_dir)+'/y_train.npy')
y_val = np.load(os.path.expanduser(data_dir)+'/y_val.npy')

print('train', y_train.shape, x_train.shape)
print('val', y_val.shape, x_val.shape)

# only use nc for training autoencoder
nc_inds_train = np.where((x_train[:,0]==1))[0]
x_train_nc = x_train[nc_inds_train, :] 
y_train_nc = y_train[nc_inds_train, :] 

nc_inds_val = np.where((x_val[:,0]==1))[0]
x_val_nc = x_val[nc_inds_val, :] 
y_val_nc = y_val[nc_inds_val, :] 

print('train nc', y_train_nc.shape, x_train_nc.shape)
print('val nc', y_val_nc.shape, x_val_nc.shape)


num_train = y_train_nc.shape[0]
num_val = y_val_nc.shape[0]

x_cam = x_train_nc[0:num_train, 1:425]
x_lid = x_train_nc[0:num_train, 425:]

y_cam = y_train_nc[0:num_train, 1:425]
y_lid = y_train_nc[0:num_train, 425:]


X = [x_cam, x_lid]
Y = [y_cam, y_lid]


X_validation = [x_val_nc[0:num_val, 1:425], x_val_nc[0:num_val, 425:]]
Y_validation = [y_val_nc[0:num_val, 1:425], y_val_nc[0:num_val, 425:]]



print('input',X[0].shape, X[1].shape, Y[0].shape, Y[1].shape)
print('validation',X_validation[0].shape, X_validation[1].shape, Y_validation[0].shape, Y_validation[1].shape)




# # Train the model, and save metrics into history object
# ###############################################################
earlyStopping = EarlyStopping(monitor='val_loss', patience=10, verbose=0, mode='min')
mcp_save = ModelCheckpoint(os.path.expanduser(to_dir+'/models/' + settings_name ), 
                    save_best_only=True,
                    monitor='val_loss', 
                    mode='min', 
                    save_weights_only=False)

history = model.fit(X,Y, 
                    shuffle=True, epochs=n_epochs, verbose=2, batch_size=5000,          # Set verbose to 0 to supress all prints
                    validation_data=(X_validation, Y_validation),
                    callbacks=[earlyStopping, mcp_save],
                    max_queue_size=1,
                    use_multiprocessing=False, 
                    workers=1)




np.save(os.path.expanduser(to_dir) + '/history/' + settings_name, history.history)

# ###############################################################
# Anomaly finding

# ###############################################################


num_train_det = y_train.shape[0]
num_val_det = y_val.shape[0]

x_cam = x_train[0:num_train_det, 1:425]
x_lid = x_train[0:num_train_det, 425:]

pred_cam_train, pred_lid_train = predict_input(model, x_cam, x_lid)

X_det = [pred_cam_train, pred_lid_train]
Y_det = to_categorical(x_train[0:num_train_det, :1]-1, num_classes=4)


pred_cam_val, pred_lid_val = predict_input(model, x_val[0:num_val_det, 1:425], x_val[0:num_val_det, 425:])


X_val_det = [pred_cam_val, pred_lid_val]
Y_val_det = to_categorical(x_val[0:num_train_det, :1]-1, num_classes=4)


print('------------Detection module-----------------')
print('input',x_cam.shape, x_lid.shape, Y_det.shape)
print('validation',X_val_det[0].shape, X_val_det[1].shape, Y_val_det.shape)
print('---------------------------------------------')



# ###############################################################
# Train detector part


model_detect = build_detector(cam_dim, lid_dim, det_dim)
plot_model(model_detect, to_file=os.path.expanduser(to_dir)+'/model_plots/detector_'+str(layer_type)+'_'+str(size)+'_'+str(arch)+'.png', show_shapes=True, show_layer_names=True)


model_detect.compile(optimizer=optimizer, 
        loss='categorical_crossentropy',
        metrics=['categorical_accuracy','MeanSquaredError','Recall', 'Precision'])

mcp_save_detect = ModelCheckpoint(os.path.expanduser(to_dir+'/models/' + 'detector_' + settings_name), 
                    save_best_only=True,
                    monitor='val_loss', 
                    mode='min', 
                    save_weights_only=False)

history_detect = model_detect.fit(X_det,Y_det, 
                shuffle=True, epochs=n_epochs, verbose=2, batch_size=5000,          # Set verbose to 0 to supress all prints
                validation_data=(X_val_det, Y_val_det),
                callbacks=[earlyStopping, mcp_save_detect],
                max_queue_size=1,
                use_multiprocessing=False, 
                workers=1)

np.save(os.path.expanduser(to_dir) + '/history/detector_' + settings_name, history_detect.history)
# ###############################################################




