#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import pathlib





##########################################
# Plot scaled input samples
##########################################

def plot_4_samples(arr, save_dir, title, save = False):

    i_l = 1
    i_c = 425

    pathlib.Path(save_dir).mkdir(parents=True, exist_ok=True) 

    # Generate x axis
    ##########################################
    rads_lid = np.arange(-3.1241390705108643, 3.1415927410125732+0.01745329238474369 ,  0.01745329238474369)
    rads_cam = np.arange(-0.7669617533683777, 0.7851662635803223+0.0036693334113806486 ,  0.0036693334113806486)

    # Figure
    ##########################################
    fig = plt.figure(title, figsize=(15,10))

    # No error
    fig.add_subplot(141, projection='polar')
    plt.plot(rads_cam, arr[0][i_l:i_c])
    plt.plot(rads_lid, arr[0][i_c:])
    plt.title('No error')
    plt.legend(['camera', 'lidar'])

    # camera error
    fig.add_subplot(142, projection='polar')
    plt.plot(rads_cam, arr[1][i_l:i_c])
    plt.plot(rads_lid, arr[1][i_c:])
    plt.title('Camera error')
    plt.legend(['camera', 'lidar'])

    # Lidar error
    fig.add_subplot(143, projection='polar')
    plt.plot(rads_cam, arr[2][i_l:i_c])
    plt.plot(rads_lid, arr[2][i_c:])
    plt.title('Lidar error')
    plt.legend(['camera', 'lidar'])

    # Both errors
    fig.add_subplot(144, projection='polar')
    plt.plot(rads_cam, arr[3][i_l:i_c])
    plt.plot(rads_lid, arr[3][i_c:])
    plt.title('Both errors')
    plt.legend(['camera', 'lidar'])



    if save:
        plt.savefig(save_dir + '/' + title + '.jpg')
    else:
        plt.show()


def plot_4_samples_with_pred(arr, save_dir, title, save = False):

    

    pathlib.Path(save_dir).mkdir(parents=True, exist_ok=True) 

    # Generate x axis
    ##########################################
    rads_lid = np.arange(-3.1241390705108643, 3.1415927410125732+0.01745329238474369 ,  0.01745329238474369)
    rads_cam = np.arange(-0.7669617533683777, 0.7851662635803223+0.0036693334113806486 ,  0.0036693334113806486)

    rads_lid = np.reshape(rads_lid, (360,))
    rads_cam = np.reshape(rads_cam, (424,))

    # Figure
    ##########################################
    fig = plt.figure(title, figsize=(15,10))

    # No error
    fig.add_subplot(241, projection='polar')
    plt.plot(rads_cam, arr[0][1][0,:])
    plt.plot(rads_lid, arr[0][2][0,:])
    plt.title('No error')
    plt.legend(['camera', 'lidar'])

    # camera error
    fig.add_subplot(242, projection='polar')
    plt.plot(rads_cam, arr[1][1][0,:])
    plt.plot(rads_lid, arr[1][2][0,:])
    plt.title('Camera error')
    plt.legend(['camera', 'lidar'])

    # Lidar error
    fig.add_subplot(243, projection='polar')
    plt.plot(rads_cam, arr[2][1][0,:])
    plt.plot(rads_lid, arr[2][2][0,:])
    plt.title('Lidar error')
    plt.legend(['camera', 'lidar'])

    # Both errors
    fig.add_subplot(244, projection='polar')
    plt.plot(rads_cam, arr[3][1][0,:])
    plt.plot(rads_lid, arr[3][2][0,:])
    plt.title('Both errors')
    plt.legend(['camera', 'lidar'])

    fig.add_subplot(245)
    plt.plot(arr[0][0][0,:])
    plt.title('classification')

    fig.add_subplot(246)
    plt.plot(arr[1][0][0,:])
    plt.title('classification')

    fig.add_subplot(247)
    plt.plot(arr[2][0][0,:])
    plt.title('classification')

    fig.add_subplot(248)
    plt.plot(arr[3][0][0,:])
    plt.title('classification')



    if save:
        plt.savefig(save_dir + '/' + title + '.jpg')
    else:
        plt.show()

def plot_4_samples_with_pred_221(arr, save_dir, title, save = False):



    pathlib.Path(save_dir).mkdir(parents=True, exist_ok=True) 

    # Generate x axis
    ##########################################
    rads_lid = np.arange(-3.1241390705108643, 3.1415927410125732+0.01745329238474369 ,  0.01745329238474369)
    rads_cam = np.arange(-0.7669617533683777, 0.7851662635803223+0.0036693334113806486 ,  0.0036693334113806486)

    rads_lid = np.reshape(rads_lid, (360,))
    rads_cam = np.reshape(rads_cam, (424,))

    # Figure
    ##########################################
    fig = plt.figure(title, figsize=(15,10))

    # No error
    fig.add_subplot(241, projection='polar')
    plt.plot(rads_cam, arr[0][1][0][0,:])
    plt.plot(rads_lid, arr[0][1][1][0,:])
    plt.title('No error')
    plt.legend(['camera', 'lidar'])

    # camera error
    fig.add_subplot(242, projection='polar')
    plt.plot(rads_cam, arr[1][1][0][0,:])
    plt.plot(rads_lid, arr[1][1][1][0,:])
    plt.title('Camera error')
    plt.legend(['camera', 'lidar'])

    # Lidar error
    fig.add_subplot(243, projection='polar')
    plt.plot(rads_cam, arr[2][1][0][0,:])
    plt.plot(rads_lid, arr[2][1][1][0,:])
    plt.title('Lidar error')
    plt.legend(['camera', 'lidar'])

    # Both errors
    fig.add_subplot(244, projection='polar')
    plt.plot(rads_cam, arr[3][1][0][0,:])
    plt.plot(rads_lid, arr[3][1][1][0,:])
    plt.title('Both errors')
    plt.legend(['camera', 'lidar'])

    # Classification
    # [[nc_label, rec_nc], [cam_label, rec_cam], [lid_label, rec_lid], [both_label, rec_both]]
    fig.add_subplot(245)
    plt.plot(arr[0][0][0,:])
    plt.title('classification')

    fig.add_subplot(246)
    plt.plot(arr[1][0][0,:])
    plt.title('classification')

    fig.add_subplot(247)
    plt.plot(arr[2][0][0,:])
    plt.title('classification')

    fig.add_subplot(248)
    plt.plot(arr[3][0][0,:])
    plt.title('classification')



    if save:
        plt.savefig(save_dir + '/' + title + '.jpg')
    else:
        plt.show()



def predict_input(model, x_cam, x_lid):
    errors_cam = []
    errors_lid = []

    reconstructed = model.predict([x_cam, x_lid])


    for (orig, recon) in zip(x_cam, reconstructed[0]):
        mse = (orig - recon) ** 2
        errors_cam.append(mse)

    for (orig, recon) in zip(x_lid, reconstructed[1]):
        mse = (orig - recon) ** 2
        errors_lid.append(mse)

    errors_cam = np.array(errors_cam)
    errors_lid = np.array(errors_lid)

    return errors_cam, errors_lid


