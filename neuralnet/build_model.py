#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' 
from keras.layers import Dense, Input, Concatenate, Conv1D, MaxPooling1D, UpSampling1D, Conv1DTranspose, Flatten, BatchNormalization
from keras.models import Model
import tensorflow as tf



from keras.utils.vis_utils import plot_model

def build_model(layer_type, size, arch, cam_dim, lid_dim, det_dim):
    ## Camera branch

    if layer_type == 'conv' and size == 'big' and arch == '23':
        input_cam = Input(shape=(cam_dim, 1))
        br_cam = Conv1D(64,2, input_shape=(1, cam_dim, 1), padding='causal', activation="relu", use_bias=True)(input_cam)
        br_cam = Conv1D(32,4, strides=2, padding='causal', activation="relu", use_bias=True)(br_cam)
        
        input_lid = Input(shape=(lid_dim, 1))
        br_lid = Conv1D(64,2, input_shape=(1, lid_dim, 1), padding='causal', activation="relu", use_bias=True)(input_lid)
        br_lid = Conv1D(32,4, strides=2,padding='causal', activation="relu", use_bias=True)(br_lid)
        
        combined = Concatenate(axis=1)([br_cam, br_lid])
        middle = Conv1D(32,2, padding='causal', activation="relu", use_bias=True)(combined)
        middle = Conv1D(32,4, padding='causal', activation="relu", use_bias=True)(middle)
    
        dec_cam = Conv1DTranspose(32,2, padding='valid', activation="relu", use_bias=True)(middle)
        dec_cam = Conv1DTranspose(64,2, padding='valid', activation="relu", use_bias=True)(dec_cam)
        dec_cam = Flatten()(dec_cam)
        dec_cam = Dense(1000, activation="relu", use_bias=True)(dec_cam) 
        dec_cam = Dense(cam_dim, name='cam_output', activation="linear", use_bias=True)(dec_cam)
        
        dec_lid = Conv1D(32,2, padding='causal', activation="relu", use_bias=True)(middle)
        dec_lid = Conv1D(64,2, padding='causal', activation="relu", use_bias=True)(dec_lid)
        dec_lid = Flatten()(dec_lid)
        dec_lid = Dense(1000, activation="relu", use_bias=True)(dec_lid) 
        dec_lid = Dense(lid_dim, name='lid_output', activation="linear", use_bias=True)(dec_lid)

        detection = Conv1D(10,3, strides=2, padding='causal', activation="relu", use_bias=True)(middle)
        detection = Flatten()(detection)
        detection = Dense(10, activation="relu", use_bias=True)(detection) 
        detection = Dense(det_dim, activation="softmax",use_bias=True, name='detection_output')(detection) 
        model = Model(inputs=[input_cam, input_lid], outputs=[detection, dec_cam, dec_lid])

    ##########################################################################

    if layer_type == 'conv' and size == 'small' and arch == '23':
        input_cam = Input(shape=(cam_dim, 1))
        br_cam = Conv1D(16,2, input_shape=(1, cam_dim, 1), padding='causal', activation="relu", use_bias=True)(input_cam)
        br_cam = Conv1D(8,4, strides=2, padding='causal', activation="relu", use_bias=True)(br_cam)
        
        input_lid = Input(shape=(lid_dim, 1))
        br_lid = Conv1D(16,2, input_shape=(1, lid_dim, 1), padding='causal', activation="relu", use_bias=True)(input_lid)
        br_lid = Conv1D(8,4, strides=2,padding='causal', activation="relu", use_bias=True)(br_lid)
        
        combined = Concatenate(axis=1)([br_cam, br_lid])
        middle = Conv1D(16,2, padding='causal', activation="relu", use_bias=True)(combined)
    
        dec_cam = Conv1DTranspose(8,2, padding='valid', activation="relu", use_bias=True)(middle)
        dec_cam = Conv1DTranspose(16,2, padding='valid', activation="relu", use_bias=True)(dec_cam)
        dec_cam = Flatten()(dec_cam)
        dec_cam = Dense(1000, activation="relu", use_bias=True)(dec_cam) 
        dec_cam = Dense(cam_dim, name='cam_output', activation="linear", use_bias=True)(dec_cam)
        
        dec_lid = Conv1D(8,2, padding='causal', activation="relu", use_bias=True)(middle)
        dec_lid = Conv1D(16,2, padding='causal', activation="relu", use_bias=True)(dec_lid)
        dec_lid = Flatten()(dec_lid)
        dec_lid = Dense(1000, activation="relu", use_bias=True)(dec_lid) 
        dec_lid = Dense(lid_dim, name='lid_output', activation="linear", use_bias=True)(dec_lid)

        detection = Conv1D(10,1, strides=2, padding='causal', activation="relu", use_bias=True)(middle)
        detection = Flatten()(detection)
        detection = Dense(10, activation="relu", use_bias=True)(detection) 
        detection = Dense(det_dim, activation="softmax",use_bias=True, name='detection_output')(detection) 

        model = Model(inputs=[input_cam, input_lid], outputs=[detection, dec_cam, dec_lid])

    ##########################################################################

    if layer_type == 'dense' and size == 'big' and arch == '23':

        input_cam = Input(shape=(cam_dim,))
        br_cam = Dense(400, activation="relu", use_bias=True)(input_cam) 
        br_cam = Dense(300, activation="relu", use_bias=True)(br_cam) 
        br_cam = Dense(200, activation="relu", use_bias=True)(br_cam) 
        br_cam = Dense(100, activation="relu", use_bias=True)(br_cam) 
        br_cam = Dense(50, activation="relu", use_bias=True)(br_cam) 

        input_lid = Input(shape=(lid_dim,))
        br_lid = Dense(300, activation="relu", use_bias=True)(input_lid) 
        br_lid = Dense(200, activation="relu", use_bias=True)(br_lid) 
        br_lid = Dense(100, activation="relu", use_bias=True)(br_lid) 
        br_lid = Dense(80, activation="relu", use_bias=True)(br_lid) 
        br_lid = Dense(50, activation="relu", use_bias=True)(br_lid) 
    
        combined = Concatenate(axis=1)([br_cam, br_lid])
        middle = Dense(60, activation="relu", use_bias=True)(combined) 
        middle = Dense(60, activation="relu", use_bias=True)(middle) 
    
        dec_cam = Dense(50, activation="relu", use_bias=True)(middle)
        dec_cam = Dense(100, activation="relu", use_bias=True)(dec_cam) 
        dec_cam = Dense(200, activation="relu", use_bias=True)(dec_cam)
        dec_cam = Dense(300, activation="relu", use_bias=True)(dec_cam)
        dec_cam = Dense(400, activation="relu", use_bias=True)(dec_cam)  
        dec_cam = Dense(cam_dim, name='cam_output', activation="linear", use_bias=True)(dec_cam)
        
        dec_lid = Dense(50, activation="relu", use_bias=True)(middle)
        dec_lid = Dense(80, activation="relu", use_bias=True)(dec_lid) 
        dec_lid = Dense(100, activation="relu", use_bias=True)(dec_lid)
        dec_lid = Dense(200, activation="relu", use_bias=True)(dec_lid)
        dec_lid = Dense(300, activation="relu", use_bias=True)(dec_lid)  
        dec_lid = Dense(lid_dim, name='lid_output', activation="linear", use_bias=True)(dec_lid)

        detection = Dense(50, activation="relu", use_bias=True)(middle) 
        detection = Dense(10, activation="relu", use_bias=True)(detection) 
        detection = Dense(5, activation="relu", use_bias=True)(detection) 
        detection = Dense(det_dim, activation="softmax", name='detection_output')(detection) 

        model = Model(inputs=[input_cam, input_lid], outputs=[detection, dec_cam, dec_lid])

    ##########################################################################

    if layer_type == 'dense' and size == 'small' and arch == '23':
        input_cam = Input(shape=(cam_dim,))
        br_cam = Dense(400, activation="relu", use_bias=True)(input_cam) 
        br_cam = Dense(200, activation="relu", use_bias=True)(br_cam) 
        br_cam = Dense(80, activation="relu", use_bias=True)(br_cam) 

        input_lid = Input(shape=(lid_dim,))
        br_lid = Dense(300, activation="relu", use_bias=True)(input_lid) 
        br_lid = Dense(200, activation="relu", use_bias=True)(br_lid) 
        br_lid = Dense(80, activation="relu", use_bias=True)(br_lid) 
    
        combined = Concatenate(axis=1)([br_cam, br_lid])
        middle = Dense(100, activation="relu", use_bias=True)(combined) 
    
        dec_cam = Dense(80, activation="relu", use_bias=True)(middle)
        dec_cam = Dense(200, activation="relu", use_bias=True)(dec_cam) 
        dec_cam = Dense(400, activation="relu", use_bias=True)(dec_cam)  
        dec_cam = Dense(cam_dim, name='cam_output', activation="linear", use_bias=True)(dec_cam)
        
        dec_lid = Dense(80, activation="relu", use_bias=True)(middle)
        dec_lid = Dense(200, activation="relu", use_bias=True)(dec_lid) 
        dec_lid = Dense(300, activation="relu", use_bias=True)(dec_lid)  
        dec_lid = Dense(lid_dim, name='lid_output', activation="linear", use_bias=True)(dec_lid)

        detection = Dense(40, activation="relu", use_bias=True)(middle) 
        detection = Dense(20, activation="relu", use_bias=True)(detection) 
        detection = Dense(5, activation="relu", use_bias=True)(detection) 
        detection = Dense(det_dim, activation="softmax", name='detection_output')(detection) 

        model = Model(inputs=[input_cam, input_lid], outputs=[detection, dec_cam, dec_lid])

    ##########################################################################


    return model

# layer_type = 'dense'  #'conv' or 'dense'
# size = 'small'  #'small' or 'big'
# arch = '221'  #'23' or '221'
# to_dir = '~/network'

# model = build_model(layer_type, size, arch, 424, 360, 4)
# plot_model(model, to_file=os.path.expanduser(to_dir)+'/model_plots/'+str(layer_type)+'_'+str(size)+'_'+str(arch)+'.png', show_shapes=True, show_layer_names=True)
