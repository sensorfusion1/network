from sklearn.preprocessing import Normalizer, MinMaxScaler, QuantileTransformer, RobustScaler
import numpy as np
from pickle import load
import os

to_dir = '~/labeling/training'


settings = np.load(os.path.expanduser('~/labeling/training/')+'settings.npy', allow_pickle=True).item() 

only_nc = settings['only_nc']
augment_noise = settings['augment_noise']
augment = settings['augment']
mode = settings['mode']

layer_type = 'dense'  #'conv' or 'dense'
size = 'small'  #'small' or 'big'
arch = '23'  #'23' or '221'
cam_dim = 424
lid_dim = 360
det_dim = 4
n_epochs = 5

string = str(layer_type) + '_' + str(size) + '_' + str(arch) + '_' + str(n_epochs) + 'epochs_' + str(only_nc) + str(augment_noise) + str(augment) + str(mode)

print(string)
