#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' 
import keras
import time
from pickle import load
from sklearn.preprocessing import RobustScaler, Normalizer
from plot_4_samples import plot_4_samples, plot_4_samples_with_pred, predict_input, plot_4_samples_with_pred_221



print('Loading net for visualization')

##########################################
# settings
##########################################

data_dir = os.path.expanduser('~/labeling/training')
network_dir = os.path.expanduser('~/network')


settings = np.load(data_dir+'/settings.npy', allow_pickle=True).item() 

only_nc = settings['only_nc']
augment_noise = settings['augment_noise']
augment = settings['augment']
mode = 'quant_input' #settings['mode']

layer_type = settings['layer_type'] 
size = settings['size'] 
arch = '221' #settings['arch']
n_epochs = settings['n_epochs']
save = True


cam_dim = 424
lid_dim = 360
det_dim = 4


settings_name = str(layer_type) + '_' + str(size) + '_' + str(arch) + '_' + str(n_epochs) + 'epochs_' + str(only_nc) + str(augment_noise) + str(augment) + str(mode)

save_dir = network_dir + '/plots/' + settings_name 

print('settings: ', settings)

i_l = 1
i_c = 425

##########################################
# Load data
##########################################
x_val = np.load(data_dir + '/x_val.npy')
y_val = np.load(data_dir + '/y_val.npy')


print(x_val.shape, y_val.shape)

print(x_val[0, 0:5])
print(y_val[0, 0:5])



# Load models #
##########################################
model = keras.models.load_model(network_dir + '/models/' + settings_name)
history = np.load(network_dir + '/history/' + settings_name + '.npy',allow_pickle='TRUE').item()


##########################################
# Select data
##########################################
# only use x_val: already scaled
print('--------Getting scaled x samples----------')
nc_sample_ind = np.where(x_val[:,0]==1)[0]
nc_sample = x_val[nc_sample_ind[0], :]
print(nc_sample[0:5])

cam_error_ind = np.where(x_val[:,0]==2)[0]
cam_sample = x_val[cam_error_ind[0], :]
print(cam_sample[0:5])

lid_error_ind = np.where(x_val[:,0] ==3)[0]
lid_sample = x_val[lid_error_ind[0], :]
print(lid_sample[0:5])

both_errors_ind = np.where(x_val[:,0] ==4)[0]
both_sample = x_val[both_errors_ind[0], :]
print(both_sample[0:5])

plot_4_samples([nc_sample, cam_sample, lid_sample, both_sample], save_dir , 'scaled_x_samples', save = save)



##########################################
# Get non scaled data (y_val)
##########################################
# only use x_val: already scaled
print('--------Getting unscaled, clean y samples----------')
y_nc_sample = y_val[nc_sample_ind[0], :]
print(y_nc_sample[0:5])

y_cam_sample = y_val[cam_error_ind[0], :]
print(y_cam_sample[0:5])

y_lid_sample = y_val[lid_error_ind[0], :]
print(y_lid_sample[0:5])

y_both_sample = y_val[both_errors_ind[0], :]
print(y_both_sample[0:5])

plot_4_samples([y_nc_sample, y_cam_sample, y_lid_sample,y_both_sample], save_dir , 'clean_y_samples', save = save)



##########################################
# Predict data
##########################################
# only use x_val: already scaled
print('--------Prediction on x samples----------')


model_detect = keras.models.load_model(network_dir + '/models/detector_' + settings_name)
history_detect = np.load(network_dir + '/history/detector_' + settings_name + '.npy',allow_pickle='TRUE').item()

rec_nc = model.predict([np.reshape(nc_sample[i_l:i_c], (1,424)), np.reshape(nc_sample[i_c:], (1,360))])
nc_cam, nc_lid = predict_input(model, np.reshape(nc_sample[i_l:i_c], (1,424)), np.reshape(nc_sample[i_c:], (1,360)))
nc_label = model_detect.predict([nc_cam, nc_lid])

rec_cam = model.predict([np.reshape(cam_sample[i_l:i_c], (1,424)), np.reshape(cam_sample[i_c:], (1,360))])
cam_cam, cam_lid = predict_input(model, np.reshape(cam_sample[i_l:i_c], (1,424)), np.reshape(cam_sample[i_c:], (1,360)))
cam_label = model_detect.predict([cam_cam, cam_lid])

rec_lid = model.predict([np.reshape(lid_sample[i_l:i_c], (1,424)), np.reshape(lid_sample[i_c:], (1,360))])
lid_cam, lid_lid = predict_input(model, np.reshape(lid_sample[i_l:i_c], (1,424)), np.reshape(lid_sample[i_c:], (1,360)))
lid_label = model_detect.predict([lid_cam, lid_lid])

rec_both = model.predict([np.reshape(both_sample[i_l:i_c], (1,424)), np.reshape(both_sample[i_c:], (1,360))])
both_cam, both_lid = predict_input(model, np.reshape(both_sample[i_l:i_c], (1,424)), np.reshape(both_sample[i_c:], (1,360)))
both_label = model_detect.predict([both_cam, both_lid])

plot_4_samples_with_pred_221([[nc_label, rec_nc], [cam_label, rec_cam], 
        [lid_label, rec_lid], [both_label, rec_both]], save_dir , 'reconstructed_samples', save = save)

print(nc_label, cam_label, lid_label, both_label)
# ##########################################
# # Plot the metrics
# ##########################################


print(history.keys())
print(history_detect.keys())

fig = plt.figure('Losses', figsize=(20,10))
plt.subplot(2,3,1)
plt.plot(history['cam_output_loss'])
plt.plot(history['val_cam_output_loss'])
plt.legend(['train', 'val'])
plt.title('Camera loss')
plt.ylabel('loss')

plt.subplot(2,3,2)
plt.plot(history['lid_output_loss'])
plt.plot(history['val_lid_output_loss'])
plt.legend(['train', 'val'])
plt.title('Lidar loss')

plt.subplot(2,3,3)
plt.plot(history_detect['loss'])
plt.plot(history_detect['val_loss'])
plt.title('Detection loss')
plt.legend(['train', 'val'])

plt.subplot(2,3,4)
plt.plot(history['cam_output_mean_squared_error'])
plt.plot(history['val_cam_output_mean_squared_error'])
plt.legend(['train', 'val'])
plt.title('Camera mse')
plt.ylabel('MSE')
plt.xlabel('epoch')

plt.subplot(2,3,5)
plt.plot(history['lid_output_mean_squared_error'])
plt.plot(history['val_lid_output_mean_squared_error'])
plt.title('Lidar mse')
plt.xlabel('epoch')
plt.legend(['train', 'val'])

plt.subplot(2,3,6)
plt.plot(history_detect['mean_squared_error'])
plt.plot(history_detect['val_mean_squared_error'])
plt.legend(['train', 'val'])
plt.title('Detection mse')
plt.xlabel('epoch')
if save:
    plt.savefig(save_dir+'/losses.jpg')
else:
    plt.show()



fig = plt.figure('Detection', figsize=(20,10))
plt.subplot(2,2,1)
plt.plot(history_detect['categorical_accuracy'])
plt.plot(history_detect['val_categorical_accuracy'])
plt.legend(['train', 'val'])
plt.title('categorical_accuracy')

plt.subplot(2,2,2)
plt.plot(history_detect['recall'])
plt.plot(history_detect['val_recall'])
plt.legend(['train', 'val'])
plt.title('recall')

plt.subplot(2,2,3)
plt.plot(history_detect['loss'])
plt.legend(['train', 'val'])
plt.title('loss')

plt.subplot(2,2,4)
plt.plot(history_detect['precision'])
plt.plot(history_detect['val_precision'])
plt.legend(['train', 'val'])
plt.title('precision')
plt.xlabel('epoch')


if save:
    plt.savefig(save_dir+'/detection.jpg')
else:
    plt.show()

