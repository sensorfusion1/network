#!/usr/bin/env python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' 
from keras.layers import Dense, Input, Concatenate, Conv1D, MaxPooling1D, UpSampling1D, Conv1DTranspose, Flatten, BatchNormalization
from keras.models import Model
import tensorflow as tf



from keras.utils.vis_utils import plot_model

def build_model(layer_type, size, arch, cam_dim, lid_dim, det_dim):

    ##########################################################################

    if layer_type == 'conv' and size == 'big' and arch == '221':
        input_cam = Input(shape=(cam_dim, 1))
        br_cam = Conv1D(64,2, input_shape=(1, cam_dim, 1), padding='causal', activation="relu", use_bias=True)(input_cam)
        br_cam = Conv1D(32,4, strides=2, padding='causal', activation="relu", use_bias=True)(br_cam)
        
        input_lid = Input(shape=(lid_dim, 1))
        br_lid = Conv1D(64,2, input_shape=(1, lid_dim, 1), padding='causal', activation="relu", use_bias=True)(input_lid)
        br_lid = Conv1D(32,4, strides=2,padding='causal', activation="relu", use_bias=True)(br_lid)
        
        combined = Concatenate(axis=1)([br_cam, br_lid])
        middle = Conv1D(32,2, padding='causal', activation="relu", use_bias=True)(combined)
        middle = Conv1D(32,4, padding='causal', activation="relu", use_bias=True)(middle)
    
        dec_cam = Conv1DTranspose(32,2, padding='valid', activation="relu", use_bias=True)(middle)
        dec_cam = Conv1DTranspose(64,2, padding='valid', activation="relu", use_bias=True)(dec_cam)
        dec_cam = Flatten()(dec_cam)
        dec_cam = Dense(1000, activation="relu", use_bias=True)(dec_cam) 
        dec_cam = Dense(cam_dim, name='cam_output', activation="linear", use_bias=True)(dec_cam)
        
        dec_lid = Conv1D(32,2, padding='causal', activation="relu", use_bias=True)(middle)
        dec_lid = Conv1D(64,2, padding='causal', activation="relu", use_bias=True)(dec_lid)
        dec_lid = Flatten()(dec_lid)
        dec_lid = Dense(1000, activation="relu", use_bias=True)(dec_lid) 
        dec_lid = Dense(lid_dim, name='lid_output', activation="linear", use_bias=True)(dec_lid)


        model = Model(inputs=[input_cam, input_lid], outputs=[dec_cam, dec_lid])


    ##########################################################################

    if layer_type == 'conv' and size == 'small' and arch == '221':
        input_cam = Input(shape=(cam_dim, 1))
        br_cam = Conv1D(16,2, input_shape=(1, cam_dim, 1), padding='causal', activation="relu", use_bias=True)(input_cam)
        br_cam = Conv1D(8,4, strides=2, padding='causal', activation="relu", use_bias=True)(br_cam)
        
        input_lid = Input(shape=(lid_dim, 1))
        br_lid = Conv1D(16,2, input_shape=(1, lid_dim, 1), padding='causal', activation="relu", use_bias=True)(input_lid)
        br_lid = Conv1D(8,4, strides=2,padding='causal', activation="relu", use_bias=True)(br_lid)
        
        combined = Concatenate(axis=1)([br_cam, br_lid])
        middle = Conv1D(16,2, padding='causal', activation="relu", use_bias=True)(combined)
    
        dec_cam = Conv1DTranspose(8,2, padding='valid', activation="relu", use_bias=True)(middle)
        dec_cam = Conv1DTranspose(16,2, padding='valid', activation="relu", use_bias=True)(dec_cam)
        dec_cam = Flatten()(dec_cam)
        dec_cam = Dense(1000, activation="relu", use_bias=True)(dec_cam) 
        dec_cam = Dense(cam_dim, name='cam_output', activation="linear", use_bias=True)(dec_cam)
        
        dec_lid = Conv1D(8,2, padding='causal', activation="relu", use_bias=True)(middle)
        dec_lid = Conv1D(16,2, padding='causal', activation="relu", use_bias=True)(dec_lid)
        dec_lid = Flatten()(dec_lid)
        dec_lid = Dense(1000, activation="relu", use_bias=True)(dec_lid) 
        dec_lid = Dense(lid_dim, name='lid_output', activation="linear", use_bias=True)(dec_lid)



        model = Model(inputs=[input_cam, input_lid], outputs=[dec_cam, dec_lid])

    ##########################################################################

    if layer_type == 'dense' and size == 'big' and arch == '221':

        input_cam = Input(shape=(cam_dim,))
        br_cam = Dense(400, activation="relu", use_bias=True)(input_cam) 
        br_cam = Dense(300, activation="relu", use_bias=True)(br_cam) 
        br_cam = Dense(200, activation="relu", use_bias=True)(br_cam) 
        br_cam = Dense(100, activation="relu", use_bias=True)(br_cam) 
        br_cam = Dense(50, activation="relu", use_bias=True)(br_cam) 

        input_lid = Input(shape=(lid_dim,))
        br_lid = Dense(300, activation="relu", use_bias=True)(input_lid) 
        br_lid = Dense(200, activation="relu", use_bias=True)(br_lid) 
        br_lid = Dense(100, activation="relu", use_bias=True)(br_lid) 
        br_lid = Dense(80, activation="relu", use_bias=True)(br_lid) 
        br_lid = Dense(50, activation="relu", use_bias=True)(br_lid) 
    
        combined = Concatenate(axis=1)([br_cam, br_lid])
        middle = Dense(60, activation="relu", use_bias=True)(combined) 
        middle = Dense(60, activation="relu", use_bias=True)(middle) 
    
        dec_cam = Dense(50, activation="relu", use_bias=True)(middle)
        dec_cam = Dense(100, activation="relu", use_bias=True)(dec_cam) 
        dec_cam = Dense(200, activation="relu", use_bias=True)(dec_cam)
        dec_cam = Dense(300, activation="relu", use_bias=True)(dec_cam)
        dec_cam = Dense(400, activation="relu", use_bias=True)(dec_cam)  
        dec_cam = Dense(cam_dim, name='cam_output', activation="linear", use_bias=True)(dec_cam)
        
        dec_lid = Dense(50, activation="relu", use_bias=True)(middle)
        dec_lid = Dense(80, activation="relu", use_bias=True)(dec_lid) 
        dec_lid = Dense(100, activation="relu", use_bias=True)(dec_lid)
        dec_lid = Dense(200, activation="relu", use_bias=True)(dec_lid)
        dec_lid = Dense(300, activation="relu", use_bias=True)(dec_lid)  
        dec_lid = Dense(lid_dim, name='lid_output', activation="linear", use_bias=True)(dec_lid)


        model = Model(inputs=[input_cam, input_lid], outputs=[dec_cam, dec_lid])

    ##########################################################################

    if layer_type == 'dense' and size == 'small' and arch == '221':
        input_cam = Input(shape=(cam_dim,))
        br_cam = Dense(400, activation="relu", use_bias=True)(input_cam) 
        br_cam = Dense(200, activation="relu", use_bias=True)(br_cam) 
        br_cam = Dense(80, activation="relu", use_bias=True)(br_cam) 

        input_lid = Input(shape=(lid_dim,))
        br_lid = Dense(300, activation="relu", use_bias=True)(input_lid) 
        br_lid = Dense(200, activation="relu", use_bias=True)(br_lid) 
        br_lid = Dense(80, activation="relu", use_bias=True)(br_lid) 
    
        combined = Concatenate(axis=1)([br_cam, br_lid])
        middle = Dense(100, activation="relu", use_bias=True)(combined) 
    
        dec_cam = Dense(80, activation="relu", use_bias=True)(middle)
        dec_cam = Dense(200, activation="relu", use_bias=True)(dec_cam) 
        dec_cam = Dense(400, activation="relu", use_bias=True)(dec_cam)  
        dec_cam_out = Dense(cam_dim, name='cam_output', activation="linear", use_bias=True)(dec_cam)
        
        dec_lid = Dense(80, activation="relu", use_bias=True)(middle)
        dec_lid = Dense(200, activation="relu", use_bias=True)(dec_lid) 
        dec_lid = Dense(300, activation="relu", use_bias=True)(dec_lid)  
        dec_lid_out = Dense(lid_dim, name='lid_output', activation="linear", use_bias=True)(dec_lid)


        model = Model(inputs=[input_cam, input_lid], outputs=[dec_cam_out, dec_lid_out])

    ##########################################################################

    return model

def build_detector(cam_dim, lid_dim, det_dim):
    input_cam = Input(shape=(cam_dim,))
    br_cam = Dense(400, activation="relu", use_bias=True)(input_cam) 
    br_cam = Dense(200, activation="relu", use_bias=True)(br_cam) 
    br_cam = Dense(80, activation="relu", use_bias=True)(br_cam) 

    input_lid = Input(shape=(lid_dim,))
    br_lid = Dense(300, activation="relu", use_bias=True)(input_lid) 
    br_lid = Dense(200, activation="relu", use_bias=True)(br_lid) 
    br_lid = Dense(80, activation="relu", use_bias=True)(br_lid) 

    combined = Concatenate(axis=1)([br_cam, br_lid])
    detection = Dense(100, activation="relu", use_bias=True)(combined) 
    detection = Dense(10, activation="relu", use_bias=True)(detection)
    detection = Dense(det_dim, name='det_output', activation="softmax", use_bias=True)(detection)

    model = Model(inputs=[input_cam, input_lid], outputs=detection)

    return model

